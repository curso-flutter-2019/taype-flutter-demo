# taype-flutter-demo

![Taype Flutter Demo](/images/taype-flutter.png "Taype Flutter Demo")

## Acerca de

Este es un proyecto desarrollado en Flutter para el curso *Desarrollando y diseñando aplicaciones móviles con Flutter* impartido durante las jornadas *InforSanLuis 2019*.

## Equipo

 * Emmanuel Antico
 * (Lic.) Alejandro Arnaudo
 
## Proyecto

### Relato 

Para gestionar los datos de pólizas, la empresa *Taype* requiere el diseño de una aplicación movil para permitir el alta de datos de manera remota. La aplicación permite realizar el alta de una póliza en sitio a partir de un número de referencia. Una póliza puede estar asociada a 1 o más lotes. Un lote está definido por una serie de coordenadas y una *marker* principal. Tanto los datos del *marker* como las coordenadas se ingresan como un par de valores compuestos por *latitud* y *longitud*. La aplicación permite el ingreso de estos datos a mano o a través un mapa mostrando el area circundante. Por defecto, una póliza se encuentra en estado *Pendiente*. Una vez completado el ingreso de datos, su estado cambia a *Completado*.

### Stack

 * Frontend: Flutter.
 * Backend: Firebase (Auth + Cloud Firestore).

### Credenciales

Para poder loguearse a la aplicación, utilizar las siguientes credenciales:

 * E-mail: demo@taype.com.ar
 * Password: demo123
 
### Pantallas implementadas

 * Splash screen (Stateless)
 * Login
 * Lista Pólizas
 * Detalle Póliza
 * Nueva Póliza

### Casos de uso implementados

 [X] Login
 [X] Ver datos de póliza
 [X] Alta de póliza.
 [ ] Alta de lote.

## Licencia

MIT
