enum Status{
  pending ,
  blocked,
  completed
}

class Assessment {
  String id;
  String nroRef;
  Status status = Status.pending;

  Assessment([this.nroRef]);
}
