import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app/login.dart';
import 'customCard.dart';
import 'authentication.dart';
import 'new_assessment.dart';

class HomePage extends StatefulWidget {
    HomePage({this.auth, this.logoutCallback, this.title}); //update this to include the uid in the constructor
    final String title;

    final BaseAuth auth;
    final VoidCallback logoutCallback;

    @override
    _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
    TextEditingController taskTitleInputController;
    TextEditingController taskDescripInputController;
    FirebaseUser currentUser;

    @override
    initState() {
        taskTitleInputController = new TextEditingController();
        taskDescripInputController = new TextEditingController();
        this.getCurrentUser();
        super.initState();
    }

    void getCurrentUser() async {
        currentUser = await FirebaseAuth.instance.currentUser();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text(widget.title),
                actions: <Widget>[
                    FlatButton(
                        child: Text("Log Out"),
                        textColor: Colors.white,
                        onPressed: () {
                            FirebaseAuth.instance
                                .signOut()
                                .then((result) =>

                                Navigator.pushReplacement(context, MaterialPageRoute(
                                    builder: (context){
                                        return LoginPage();
                                    }
                                )))
                                .catchError((err) => print(err));
                        },
                    )
                ],
            ),
            body: Center(
                child: Container(
                    padding: const EdgeInsets.all(20.0),
                    child: StreamBuilder<QuerySnapshot>(
                        stream: Firestore.instance
                            .collection('assessments')
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (snapshot.hasError)
                                return new Text('Error: ${snapshot.error}');
                            switch (snapshot.connectionState) {
                                case ConnectionState.waiting:
                                    return new Text('Loading...');
                                default:
                                    return new ListView(
                                        children: snapshot.data.documents
                                            .map((DocumentSnapshot document) {
                                            return new CustomCard(
                                                title: document['nroRef'],
                                                description: document['status'],
                                            );
                                        }).toList(),
                                    );
                            }
                        },
                    )),
            ),
            floatingActionButton: FloatingActionButton(
                onPressed: _createAssessment,
                tooltip: 'Add',
                child: Icon(Icons.add),
            ),
        );
    }

    void _createAssessment() async {
      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
            return NewAssessment(auth: widget.auth, logoutCallback: widget.logoutCallback);
      }));
    }
}
