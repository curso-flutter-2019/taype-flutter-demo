

import 'package:flutter/material.dart';



// Splash screen widget
import 'splash.dart';


/*Este es el punto de entrada a la app, haciendo la llamada runApp pasandole como parametro nuestra futura App*/
void main() {

   runApp(MyApp());

}

/*Comenzamos a definir una pantalla básica donde será un Stateless Widget*/
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Auth Demo',
      home: SplashScreen()
    );
  }
}
