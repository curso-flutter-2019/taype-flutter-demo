import 'package:flutter/material.dart';
import 'theme.dart';
import 'authentication.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.loginCallback});

  final BaseAuth auth;
  final VoidCallback loginCallback;

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _password;

  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;

    super.initState();
  }

  @override
  Widget build(BuildContext context) => showLogin();

  Widget showLogin() {
    return MaterialApp(
      title: 'Ingresar',
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            showForm(),
            showCircularProgress()
          ]
        )
      )
    );
  }

  bool validateAndSave() {
    // this isn't even my
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }

    return false;
  }

  void _validateAndSubmit() async {
    setState(() {
        _errorMessage = "";
        _isLoading = true;
    });

    if (validateAndSave()) {
      String userId = "";

      try {
        userId = await widget.auth.signIn(_email, _password);
        debugPrint('Signed in: $userId');

        setState(() {
            _isLoading = false;
        });

        if (userId != null && userId.length > 0) {
          widget.loginCallback();
        }
      } catch (e) {
        debugPrint('Error: $e');
        setState(() {
            _isLoading = false;
            _errorMessage = e.message;
            _formKey.currentState.reset();
        });
      }
    }
  }

  Widget showLogo() {
    return Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
        child: Center(
          child: Image(image: AssetImage('assets/images/icono.png'))
        )
      )
    );
  }

  Widget showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }

    return Container(height: 0.0, width: 0.0);
  }

  Widget showEmailInput() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Email',
          icon: Icon(Icons.mail, color: Colors.grey)
        ),
        validator: (value) => value.isEmpty ? 'Ingrese su e-mail' : null,
        onSaved: (value) => _email = value.trim()
      )
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Password',
          icon: Icon(Icons.lock, color: Colors.grey)
        ),
        validator: (value) => value.isEmpty ? 'Ingrese su password' : null,
        onSaved: (value) => _password = value.trim()
      )
    );
  }

  Widget showPrimaryButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0)
          ),
          color: AppTheme.primaryColor,
          child: Text('Ingresar', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: _validateAndSubmit
        )
      )
    );
  }

  Widget showErrorMessage() {
    if (_errorMessage != null && _errorMessage.length > 0) {
      return Text(_errorMessage, style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
      ));
    }

    return Container(height: 0.0, width: 0.0);
  }

  Widget showForm() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showLogo(),
            showEmailInput(),
            showPasswordInput(),
            showPrimaryButton(),
            showErrorMessage()
          ]
        )
      )
    );
  }
}
