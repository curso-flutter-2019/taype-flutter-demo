import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'theme.dart';
import 'authentication.dart';

class NewAssessment extends StatefulWidget {
  NewAssessment({this.auth, this.logoutCallback});

  final BaseAuth auth;
  final VoidCallback logoutCallback;

  @override
  State<NewAssessment> createState() => NewAssessmentState();
}

class NewAssessmentState extends State<NewAssessment> {
  final _formKey = new GlobalKey<FormState>();

  String _nroRef;
  bool _isSaving;

  @override
  Widget build(BuildContext context) => showForm();

  @override
  void initState() {
    _isSaving = false;

    super.initState();
  }

  void _signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      debugPrint('Error: $e');
    }
  }

  Widget showForm() {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Nueva Póliza'),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Logout',
              style: TextStyle(fontSize: 17.0, color: Colors.white)
            ),
            onPressed: _signOut
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          showFormFields(),
        ]
      )
    );
  }

  Widget showFormFields() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showNroRefInput(),
            showNewAssessmentButton(),
            showCancelButton(),
            showCircularProgress()
          ]
        )
      )
    );
  }

  Widget showNroRefInput() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        // keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nro. Referencia',
          icon: Icon(Icons.info, color: Colors.grey)
        ),
        validator: (value) => value.isEmpty ? 'Ingrese un número de referencia' : null,
        onSaved: (value) => _nroRef = value.trim()
      )
    );
  }

  bool validateAndSave() {
    // this isn't even my
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }

    return false;
  }

  void _createNewAssessment() async {
    setState(() {
        _isSaving = true;
    });

    if (validateAndSave()) {
      final now = DateTime.now();
      final date = '${now.day}/${now.month}/${now.year}';

      await Firestore.instance.collection('assessments').document()
      .setData({
          'nroRef': _nroRef,
          'status': 'pending',
          'fecha': date
      });

      Navigator.pop(context);

      setState(() {
          _nroRef = "";
          _isSaving = false;
      });
    }
  }

  void _cancelAction() async {
    Navigator.pop(context);
  }

  Widget showNewAssessmentButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0)
          ),
          color: AppTheme.primaryColor,
          child: Text('Cargar Lote', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: _createNewAssessment
        )
      )
    );
  }

  Widget showCancelButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0)
          ),
          color: Colors.grey,
          child: Text('Cancelar', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: _cancelAction
        )
      )
    );
  }

  Widget showCircularProgress() {
    if (_isSaving) {
      return Center(child: CircularProgressIndicator());
    }

    return Container(height: 0.0, width: 0.0);
  }
}
