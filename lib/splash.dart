import 'package:flutter/material.dart';
import 'theme.dart';
import 'authentication.dart';
import 'root.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future<String> checkStatus() async {
  return new Future.delayed(const Duration(seconds: 3), () => "Status OK");
}

class SplashScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    debugPrint("Checking status...");

    () async {
      final result = await checkStatus();
      debugPrint(result);

      Firestore.instance
          .collection('assessments')
          .snapshots()
          .listen((data) => data.documents.forEach((doc) => debugPrint(doc["nroRef"])));

      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
            return RootPage(auth: Auth());
      }));
    }();

    return Scaffold(
      backgroundColor: AppTheme.primaryColor,
      body: Center(
        child: Image(image: AssetImage('assets/images/logo.png'))
      )
    );
  }
}
