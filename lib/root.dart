import 'package:flutter/material.dart';
import 'authentication.dart';
import 'login.dart';
import 'home.dart';

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN
}

class RootPage extends StatefulWidget {
  RootPage({this.auth});

  final BaseAuth auth;

  @override
  State<RootPage> createState() => RootPageState();
}

class RootPageState extends State<RootPage> {
  AuthStatus authStatus;
  String _userId = "";

  get key => 'Home';

  @override
  void initState() {
    authStatus = AuthStatus.NOT_DETERMINED;
    super.initState();

    widget.auth.getCurrentUser().then((user) {
        setState(() {
            if (user != null) {
              _userId = user?.uid;
            }

            authStatus = user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
        });
    });
  }

  void loginCallback() {
    widget.auth.getCurrentUser().then((user) {
        setState(() {
            _userId = user.uid.toString();
        });
    });
    setState(() {
        authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void logoutCallback() {
    setState(() {
        authStatus = AuthStatus.NOT_LOGGED_IN;
        _userId = "";
    });
  }

  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator()
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_LOGGED_IN:
      return LoginPage(auth: widget.auth, loginCallback: loginCallback);

      case AuthStatus.LOGGED_IN:
      return HomePage(auth: widget.auth, logoutCallback: logoutCallback, title: "Title");

      case AuthStatus.NOT_DETERMINED:
      default:
      return buildWaitingScreen();
    }
  }
}
